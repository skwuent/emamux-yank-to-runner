;;; emamux-yank-to-runner.el --- send car of kill ring to runner pane.

;;; Version: 0.0.0
;;; Author: NGK Sternhagen <sternagen@protonmail.ch>
;;; Keywords: tmux emamux
;;; Created: 03 January 2017

(defun emamux-yank-to-runner ()
  "send car of kill ring to runner pane"
  (interactive)
  (let ((input (substring-no-properties (car kill-ring))))
    (emamux:run-command input)))
